(function (params) {
  console.log(`Main script init`);
  const navlinks = document.querySelectorAll('.nav-link');

  navlinks.forEach(link => {
    link.addEventListener('click', function(eve) {
      eve.preventDefault();
      const targetSelector = eve.target.hash;
      const target = document.querySelector(targetSelector);
      target.scrollIntoView({ behavior: 'smooth'})
    });
  });

})();
