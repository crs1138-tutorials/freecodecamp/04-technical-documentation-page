# Changelog
All notable changes to the **04 Technical Documentation Page** will be documented in this file. I'm using [semantic versioning](https://semver.org/) and the structure of this changelog is based on [keepachangelog.com](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
This section keeps track of upcoming changes and gives you a notice about what features you can expect in future.


## [1.0.0] - 2019-05-19
The first release of Technical Documentation Page project aims to satisfy the requirements for HTML+CSS certification of FreeCodeCamp.

### Added

- adds the brief in the [README](./README.md) for the project
- defines the HTML structure and content using the [MDN's HTML Elements reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Element) guide as the content provider
- styles the page to match FCC's requirement and provide visually pleasing user experience (heavilly inspired by MDN 😉)
- adds Javascript functionality for scrolling into view the part of content as selected by the user in the navigation menu